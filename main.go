package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/aws/signer/v4"
)

func logRequest(req *http.Request) {
	fmt.Println("Request:")
	fmt.Printf("Method: %s\n", req.Method)
	fmt.Printf("URL: %s\n", req.URL.String())
	fmt.Println("Headers:")
	for key, values := range req.Header {
		for _, value := range values {
			fmt.Printf("%s: %s\n", key, value)
		}
	}
}

func logResponse(resp *http.Response) {
	fmt.Println("Response:")
	fmt.Printf("Status: %s\n", resp.Status)
	fmt.Println("Headers:")
	for key, values := range resp.Header {
		for _, value := range values {
			fmt.Printf("%s: %s\n", key, value)
		}
	}
}

func main() {
	endpoint := flag.String("endpoint", "https://s3.cern.ch", "S3 endpoint to make query against")
	region := flag.String("region", "us-east-1", "S3 region")
	debug := flag.Bool("debug", false, "Enable debug logging")
	json := flag.Bool("json", false, "Use JSON output (default XML)")

	flag.Parse()

	if *endpoint == "" {
		log.Fatal("You must specify an S3 endpoint")
	}

	session, err := session.NewSession(&aws.Config{
		Endpoint: aws.String(*endpoint),
	})
	if err != nil {
		log.Fatalf("Failed to create session: %s", err)
	}

	endpointURL, err := url.Parse(*endpoint)
	if err != nil {
		log.Fatalf("Invalid endpoint URL: %s", err)
	}

	q := endpointURL.Query()
	q.Set("usage", "True")
	if *json {
		q.Set("format", "json")
	}
	endpointURL.RawQuery = q.Encode()

	req, _ := http.NewRequest("GET", endpointURL.String(), nil)

	signer := v4.NewSigner(session.Config.Credentials)
	_, err = signer.Sign(req, nil, "s3", *region, time.Now())
	if err != nil {
		log.Fatalf("Failed to sign request %s", err)
	}

	if *debug {
		logRequest(req)
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatalf("Failed to make request: %s", err)
	}

	defer resp.Body.Close()

	if *debug {
		logResponse(resp)
	}

	if resp.StatusCode != http.StatusOK {
		log.Fatalf("Error: got status code %d", resp.StatusCode)
	}
	// Read and print the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("Failed to read response body: %s", err)
	}

	fmt.Println(string(body))
}
