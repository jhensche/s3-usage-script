#!/bin/sh

# Run go fmt
fmt_output=$(go fmt ./...)
if [ -n "$fmt_output" ]; then
    echo "Code is not formatted. Please run 'go fmt'."
    echo "$fmt_output"
    exit 1
fi
