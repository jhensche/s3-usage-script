A simple script to check your total S3 usage. Works only on Ceph S3 implementation.

Takes the following optional parameters
```
./cephs3usage -h
Usage of ./cephs3usage:
  -debug
    	Enable debug logging
  -endpoint string
    	S3 endpoint to make query against (default "https://s3.cern.ch")
  -json
    	Enable JSON output
  -region string
    	S3 region (default "us-east-1")
```

This script assumes credentials to be configured in any way that aws cli expects. Usually ~/.aws/credentials, but mostly also the other environment variables as supported in https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html
